from flask import Flask, request, render_template
from search_engine import matching
import json
from search_engine import processed_query

app = Flask(__name__)


# ROUTING
@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/search', methods=['GET', 'POST'])
def searching():
    if request.method == 'POST':
        input_query = request.form['search_input']
        result_query =matching(input_query)
        json.dump(result_query, open('query.json', 'w'))
        f = open('query.json', )
        data = json.load(f)
        doc = []
        for i in data[0]:
            doc.append(i)
        f.close()

        return render_template('index.html', result=doc, query=input_query,speller_query=processed_query)
    else:
        render_template('index.html')


# gg = find_doc_by_id(1)


if __name__ == "__main__":
    app.run()
