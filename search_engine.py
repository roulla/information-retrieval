import string
from nltk import SpaceTokenizer, LancasterStemmer, re, wordnet, ngrams
import math
from nltk.stem.porter import PorterStemmer
import os
from nltk.stem import WordNetLemmatizer
import numpy as np
from collections import Counter
from autocorrect import Speller
import pandas as pd
import json
from irregular import dict_v2, dict_v3
from dic_abbrev import dic_abbrev
from nltk.tokenize.treebank import TreebankWordDetokenizer

# nltk.download('wordnet')
corpus_root = 'corpus'
tk = SpaceTokenizer()
stemPorter = PorterStemmer()
vectors = {}
tfs = {}
lemmatizer = WordNetLemmatizer()
lengths = Counter()
df = Counter()
postings_list = {}  # posting list storage for each token in the corpus
processed_query = " "


def get_stop_words():
    with open('stop words.txt') as f:
        lines = [line.rstrip().lower() for line in f]
    return lines


def irregular_verbs(token):
    for key, value in dict_v2.items():
        if token == value:
            new_token = key
            token = new_token
        else:
            for key1, value1 in dict_v3.items():
                if token == value1:
                    new_token_v2 = key1
                    for key2, value2 in dict_v2.items():
                        if new_token_v2 == value2:
                            new_token_v1 = key2
                            token = new_token_v1

    return token


def Abbreviation_names(token):
    for key, value in dic_abbrev.items():
        if token == value:
            new_token = key
            token = new_token
    return token


# pre-processing corpus
for filename in os.listdir(corpus_root):
    file = open(os.path.join(corpus_root, filename), "r")
    doc = file.read()
    file.close()

    # 1. Convert to lower case
    doc = doc.lower()

    # 2.process Different Date Formats
    date = re.search(
        'r"(?:\d{,2}\s)?(?:jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[a-z]*(?:-|\.|\s|,)\s?\d{,2}[a-z]*(?:-|,|\s)?\s?\d{2,4}"',
        doc)
    dates = pd.to_datetime(date)

    # 3.Remove punctuation except -
    remove = string.punctuation
    remove = remove.replace("-", "")  # don't remove hyphens
    pattern = r"[{}]".format(remove)  # create the pattern
    doc = re.sub(pattern, "", doc)

    # 4.Tokenizing with whitespace delimiter
    tokens = tk.tokenize(doc)

    stop_words = set(get_stop_words())
    # 5. Remove Stop Words
    # 6. dictionary for irregular Verbs
    tokens = [irregular_verbs(token) for token in tokens if token not in stop_words]

    # 7. stemming tokens
    # 8. dictionary for Abbreviation of names
    for index, token in enumerate(tokens):
        token = stemPorter.stem(token)
        token = Abbreviation_names(token)
        tokens[index] = token

    lengths[filename] = len(tokens)
    tf = Counter(tokens)
    df += Counter(list(set(tokens)))
    tfs[filename] = tf.copy()
    tf.clear()

# Vocabulary after processing
Vocabulary = df.keys()
# Vocabulary frequency in corpus
# print(len(Vocabulary))
# print(Vocabulary)
df_t = df.values()
# Document ID (name)
doc_name = tfs.keys()
# term frequency in document
freq_word_per_doc = tfs.values()


def idf(token):
    if df[token] == 0:
        return -1
    return math.log10(len(tfs) / df[token])
#     # IDF = log ( len(corpus) / frequency of token )


def tf(filename, token):  # TF = frequency of token in document / numbers of tokens in document (len(document))
    return 1 + math.log10(tfs[filename][token])


# loop for calculating tf-idf vectors
for filename in tfs:
    vectors[filename] = Counter()  # initializing the tf-idf vector for each doc
    length = 0
    for token in tfs[filename]:
        weight = tf(filename, token) * idf(token)  # calWeight calculates the weight of a token in a doc without normalization
        vectors[filename][token] = weight  # this is the weight of a token in a file
        length += weight ** 2  # calculating length for normalizing later
    lengths[filename] = math.sqrt(length)

# loop for normalizing weights
for filename in vectors:
    for token in vectors[filename]:
        vectors[filename][token] = vectors[filename][token] / lengths[
            filename]  # dividing weights by the document's length
        if token not in postings_list:
            postings_list[token] = Counter()
        postings_list[token][filename] = vectors[filename][token]


# store index to json file
# with open('index.json', 'w', encoding='utf-8') as f:
#     json.dump(postings_list, f, ensure_ascii=False, indent=4)
#
# # store Vector to json file
# with open('vector.json', 'w', encoding='utf-8') as f:
#     json.dump(vectors, f, ensure_ascii=False, indent=4)


# with open('index.json') as f:
#   postings_list = json.load(f)
# # print(postings_list)
#
#
# with open('vector.json') as f:
#   vectors = json.load(f)
# # print()


def matching(query_string):  # function that returns the best match for a query
    # 1. Convert to lower case
    query_string = query_string.lower()  # converting the words to lower case
    # 2. spell check
    spell = Speller()
    query_string = spell(query_string)

    # 3.process Different Date Formats
    date = re.search(
        'r"(?:\d{,2}\s)?(?:jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)[a-z]*(?:-|\.|\s|,)\s?\d{,2}[a-z]*(?:-|,|\s)?\s?\d{2,4}"',
        query_string)
    dates = pd.to_datetime(date)

    # 4.Remove punctuation except -
    remove = string.punctuation
    remove = remove.replace("-", "")  # don't remove hyphens
    pattern = r"[{}]".format(remove)  # create the pattern
    query_string = re.sub(pattern, "", query_string)

    # 5.Tokenizing with whitespace delimiter
    tokens = tk.tokenize(query_string)

    stop_words = set(get_stop_words())
    # 6. Remove Stop Words
    # 7. dictionary for irregular Verbs
    tokens = [irregular_verbs(token) for token in tokens if token not in stop_words]

    # 8. stemming tokens
    # 9. dictionary for Abbreviation of names
    for index, token in enumerate(tokens):
        token = stemPorter.stem(token)
        token = Abbreviation_names(token)
        tokens[index] = token

    # show query as string after processing
    global processed_query
    processed_query = TreebankWordDetokenizer().detokenize(tokens)
    print(processed_query)

    query_tf = {}
    query_length = 0
    flag = 0
    docs = {}
    cos_sims = Counter()
    for token in tokens:
        if token not in postings_list:
            continue
        if idf(token) == 0:
            docs[token], weights = zip(*postings_list[token].most_common())
        else:
            docs[token], weights = zip(*postings_list[token].most_common(10))  # taking top 10 in postings list
        if flag == 1:
            commondocs = set(docs[token]) & commondocs
        else:
            commondocs = set(docs[token])
            flag = 1
        query_tf[token] = 1 + math.log10(tokens.count(token))
        query_length += query_tf[token] ** 2  # calculating length for normalizing the query tf later
    query_length = math.sqrt(query_length)
    for doc in vectors:
        cos_sim = 0
        for token in query_tf:
            if doc in docs[token]:
                cos_sim = cos_sim + (query_tf[token] / query_length) * postings_list[token][doc]  # calculate actual score if document is in top 10
        cos_sims[doc] = cos_sim
    max = cos_sims.most_common(10)
    ans, weight = zip(*max)
    try:
        return ans, weight
    except UnboundLocalError:
        return "None", 0


# q=input("enter your query:")
# print(matching(q))


# Using readlines()
file1 = open('Queries.txt', 'r')
Lines = file1.readlines()
count = 1
for line in Lines:
    print("Query{}: {}".format(count, line.strip()))
    print(matching(line.strip()))
    print("\n")
    count += 1
